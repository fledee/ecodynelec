Impacts computation
===================

The final step of ``ecodynelec`` algorithm consists in coupling electricity mix information and impacts per production unit to calculate the overall impact of the 1kWh of electricity at the socket.

.. figure:: images/impacts.png
    :alt: Impacts computation
    
    *Figure 1: Impacts computation*