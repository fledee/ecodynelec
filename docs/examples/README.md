This contains the files with example notebooks converted into RST format.

We can say that
* the RST need some cleaning
* we need to change the examples.
    * The visualization can only be for the mix and/or new impacts
    * NB-convert takes all hashtags and turn it into titles, and we do not want it
    * No need for the `help()` documentation.
    * We need lots more examples than that.
