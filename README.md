# EcoDynElec: Dynamic Life Cycle Assessment of electricity for ENTSO-E countries

EcoDynElec software tracks the origin of electricity accross european countries based on generation and cross-border exchanges and allows the dynamic evaluation of environmental impacts of electricity.

![workflow](docs/images/workflow.png)

`ecodynelec` is a free software under APACHE 2.0 licence. It was developped in a collaboration between the [EMPA](https://www.empa.ch/), [HEIG-VD](https://heig-vd.ch/), the [SUPSI](https://www.supsi.ch/home.html).

## Installation
For now the software can only be installed from the [gitlab repository](https://gitlab.com/fledee/ecodynelec/)

## Documentation
An online documentation was written and is available on the [dedicated ReadTheDocs](https://ecodynelec.readthedocs.io/en/latest/)

## Contributions
EcoDynElec did contribute to the project [EcoDynBat - Ecobilan Dynamique des Bâtiments](https://www.aramis.admin.ch/Texte/?ProjectID=41804).

![logo](docs/images/logo.png)
